package mcq_proj;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class ui3 implements ActionListener{

	JFrame frame;
	JPanel panel;
	JButton bdsa,bmp,bcg,bm3,bppl,bquit;
	
	ui3()
	{
		frame=new JFrame();
		panel=new JPanel();
		bdsa=new JButton("Advanced Data Structures");
		bdsa.addActionListener(this);
		bmp=new JButton("Microprocessor");
		bmp.addActionListener(this);
		bcg=new JButton("Computer Graphics");
		bcg.addActionListener(this);
		bm3=new JButton("Engineering Mathematics");
		bm3.addActionListener(this);
		bppl=new JButton("Principles Of Programming Languages");
		bppl.addActionListener(this);
		bquit=new JButton("Quit");
		bquit.addActionListener(this);
		
		
		frame.setTitle("HOME");
		panel.add(bdsa);
		panel.add(bmp);
		panel.add(bm3);
		panel.add(bcg);
		panel.add(bppl);
		panel.add(bquit);
		
		frame.add(panel);
		frame.setSize(600, 550);
		frame.setVisible(true);
	}
	
	public static void main(String[] args)
	{
		new ui3();
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource()==bdsa)
		{
			frame.dispose();
			new ui4();
		}
		else if(e.getSource()==bmp)
		{
			frame.dispose();
			new ui4();
		}
		else if(e.getSource()==bm3)
		{
			frame.dispose();
			new ui4();
		}
		else if(e.getSource()==bcg)
		{
			frame.dispose();
			new ui4();
		}
		else if(e.getSource()==bppl)
		{
			frame.dispose();
			new ui4();
		}
		else if(e.getSource()==bquit)
		{
			frame.dispose();
			
		}
			
		
	}

}

